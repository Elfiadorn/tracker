package org.smoofy;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

import javax.servlet.annotation.WebServlet;

/**
 * Created by Shaman on 4/5/2016.
 */
@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = TrackerUI.class, productionMode = false)
public class TrackerServlet extends VaadinServlet {
}