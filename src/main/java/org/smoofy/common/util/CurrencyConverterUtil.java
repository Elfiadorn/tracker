package org.smoofy.common.util;

import org.smoofy.model.CurrencyObject;
import org.smoofy.model.CurrencyTableObject;

/**
 * Created by Shaman on 4/5/2016.
 */
public class CurrencyConverterUtil {

    public static CurrencyObject convert(String value){
        String res[] = value.split("\\s+");

        if(res.length == 2){
            CurrencyObject object = new CurrencyObject();

            //validate currency

            object.setCurrency(res[0].toUpperCase());
            try {
                object.setValue(Integer.parseInt(res[1]));
                return object;
            }catch (NumberFormatException nfe){
                return null;
            }
        }

        return null;
    }

    public static CurrencyTableObject convert(CurrencyObject object){
        CurrencyTableObject cto = new CurrencyTableObject();
        cto.setCurrency(object.getCurrency());
        cto.setCurrentValue(object.getValue());
        return cto;
    }

}
