package org.smoofy.common.eventbus;

import com.google.common.eventbus.EventBus;
import org.smoofy.common.eventbus.event.DeadEventHandler;

/**
 * Created by Shaman on 4/5/2016.
 */
public class TrackerEventBus {

    private static EventBus eventBus;

    public static EventBus getEventBus(){
        if(eventBus == null){
            eventBus = new EventBus();
            eventBus.register(new DeadEventHandler());
        }
        return eventBus;
    }

}
