package org.smoofy.common.eventbus.event;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;

/**
 * Created by Shaman on 4/5/2016.
 */
public class DeadEventHandler {

    @Subscribe
    public void deadEvent(DeadEvent deadEvent){
        //handle
    }

}
