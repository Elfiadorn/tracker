package org.smoofy.common.eventbus.event;

import org.smoofy.model.CurrencyRate;

/**
 * Created by Shaman on 4/5/2016.
 */
public class CurrencyRateAddEvent {

    private final CurrencyRate rate;

    public CurrencyRateAddEvent(CurrencyRate object){
        this.rate = object;
    }

    public CurrencyRate getRate() {
        return rate;
    }
}
