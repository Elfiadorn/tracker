package org.smoofy.common.eventbus.event;

import org.smoofy.model.CurrencyObject;

import java.util.List;

/**
 * Created by Shaman on 4/5/2016.
 */
public class SuccessfullUpload {

    private final List<CurrencyObject> result;

    public SuccessfullUpload(List<CurrencyObject> result) {
        this.result = result;
    }

    public List<CurrencyObject> getResult() {
        return result;
    }

}
