package org.smoofy.common.eventbus.event;

import org.smoofy.model.CurrencyObject;

/**
 * Created by Shaman on 4/5/2016.
 */
public class AddValueEvent {

    private CurrencyObject currencyObject;

    public AddValueEvent(CurrencyObject object){
        currencyObject = object;
    }

    public CurrencyObject getCurrencyObject() {
        return currencyObject;
    }
}
