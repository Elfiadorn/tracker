package org.smoofy.common.filter;

import com.google.common.base.Predicate;
import org.smoofy.model.CurrencyTableObject;

/**
 * Created by Shaman on 4/5/2016.
 */
public class CurrencyFilter implements Predicate<CurrencyTableObject> {

    private String currency;

    public CurrencyFilter(String currency){
        this.currency = currency.toUpperCase();
    }

    @Override
    public boolean apply(CurrencyTableObject currencyTableObject) {
        return currencyTableObject.getCurrency().toUpperCase().equals(currency);
    }

}
