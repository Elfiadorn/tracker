package org.smoofy.common.filter;

import com.google.common.base.Predicate;
import org.smoofy.model.CurrencyRate;

/**
 * Created by Shaman on 4/5/2016.
 */
public class CurrencyRateFilter implements Predicate<CurrencyRate> {

    private String currency;

    public CurrencyRateFilter(String currency){
        this.currency = currency.toUpperCase();
    }

    @Override
    public boolean apply(CurrencyRate currencyRate) {
        return currencyRate.getComparedCurrency().toUpperCase().equals(currency);
    }

}
