package org.smoofy.model;

import java.io.Serializable;

/**
 * Created by Shaman on 4/4/2016.
 */
public class CurrencyTableObject implements Serializable {

    private String currency;
    private Integer currentValue;
    private Integer inDepositValue = 0;
    private Float convertedCurrency = 0f;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(Integer currentValue) {
        this.currentValue = currentValue;
    }

    public Integer getInDepositValue() {
        return inDepositValue;
    }

    public void setInDepositValue(Integer inDepositValue) {
        this.inDepositValue = inDepositValue;
    }

    public Float getConvertedCurrency() {
        return convertedCurrency;
    }

    public void setConvertedCurrency(Float convertedCurrency) {
        this.convertedCurrency = convertedCurrency;
    }
}
