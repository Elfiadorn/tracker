package org.smoofy.model;

/**
 * Created by Shaman on 4/5/2016.
 */
public class Currency {

    private String currencyName;

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

}
