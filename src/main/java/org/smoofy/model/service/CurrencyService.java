package org.smoofy.model.service;

import com.google.common.eventbus.Subscribe;
import org.smoofy.common.eventbus.TrackerEventBus;
import org.smoofy.common.eventbus.event.AddValueEvent;
import org.smoofy.common.eventbus.event.AvailableCurrenciesChangeEvent;
import org.smoofy.model.CurrencyObject;
import org.smoofy.model.CurrencyRate;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Shaman on 4/5/2016.
 */
public class CurrencyService {

    private static CurrencyService instance;

    private static Set<String> availableCurrencies = new LinkedHashSet<String>();
    private static Map<String, CurrencyRate> rates = new HashMap<String, CurrencyRate>();

    private CurrencyService(){
        TrackerEventBus.getEventBus().register(this);
    }

    public static CurrencyService getInstance(){
        if (instance == null) {
            instance = new CurrencyService();
        }
        return instance;
    }

    public Set<String> getAvailableCurrencies() {
        return availableCurrencies;
    }

    @Subscribe
    public void addValue(AddValueEvent event) {
        CurrencyObject object = event.getCurrencyObject();

        String currency = object.getCurrency().toUpperCase();

        if(!("USD").equals(currency)){
            if(!availableCurrencies.contains(currency)){
                availableCurrencies.add(currency);

                TrackerEventBus.getEventBus().post(new AvailableCurrenciesChangeEvent());
            }
        }
    }

    public Float findRate(String currency){
        CurrencyRate rate = rates.get(currency.toUpperCase());
        if(rate != null){
            return rate.getRate();
        }
        return null;
    }

    public void insertCurrencyRate(CurrencyRate bean) {
        rates.put(bean.getComparedCurrency().toUpperCase(), bean);
    }
}
