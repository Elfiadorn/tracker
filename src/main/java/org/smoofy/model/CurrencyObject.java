package org.smoofy.model;

import java.io.Serializable;

/**
 * Created by Shaman on 4/5/2016.
 */
public class CurrencyObject implements Serializable {

    private String currency;
    private Integer value;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

}
