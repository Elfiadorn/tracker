package org.smoofy.model;

/**
 * Created by Shaman on 4/5/2016.
 */
public class CurrencyRate {

    private String currency = "USD";
    private String comparedCurrency;
    private Float rate;

    public String getCurrency() {
        return currency;
    }

    public String getComparedCurrency() {
        return comparedCurrency;
    }

    public void setComparedCurrency(String comparedCurrency) {
        this.comparedCurrency = comparedCurrency;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

}
