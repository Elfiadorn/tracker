package org.smoofy.ui;

import com.vaadin.event.UIEvents;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.smoofy.ui.console.Console;
import org.smoofy.ui.currency.CurrencyLayout;
import org.smoofy.ui.currency.rate.CurrencyRateLayout;

/**
 * Created by Shaman on 4/4/2016.
 */
public class TrackerLayout extends VerticalLayout {

    private static final String STYLE_NAME = "tracker-layout";

    private CurrencyRateLayout currencyRateLayout;
    private CurrencyLayout currencyLayout;
    private Console console;

    public TrackerLayout(){
        setSizeFull();
        addStyleName(STYLE_NAME);

        HorizontalLayout topLayout = getTopLayout();
        addComponents(topLayout, getConsole());
        setExpandRatio(topLayout, 0.8f);
        setExpandRatio(getConsole(), 0.2f);

        setSizeFull();
        setMargin(true);

        /*
        Refresher refresher = new Refresher();
        refresher.setRefreshInterval(5000);
        refresher.addListener(new Refresher.RefreshListener() {
            @Override
            public void refresh(Refresher refresher) {
                getTable().refresh();
            }
        });
        */

        UI.getCurrent().setPollInterval(5000);
        UI.getCurrent().addPollListener(new UIEvents.PollListener() {
            @Override
            public void poll(UIEvents.PollEvent event) {
                getCurrencyLayout().getTable().refresh();
            }
        });
    }

    private HorizontalLayout getTopLayout() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.addComponents(getCurrencyLayout(), getCurrencyRateLayout());
        layout.setSizeFull();
        layout.setSpacing(true);
        return layout;
    }

    public CurrencyLayout getCurrencyLayout() {
        if(currencyLayout == null){
            currencyLayout = new CurrencyLayout();
        }
        return currencyLayout;
    }

    public Console getConsole() {
        if(console == null){
            console = new Console();
        }
        return console;
    }

    public CurrencyRateLayout getCurrencyRateLayout() {
        if (currencyRateLayout == null) {
            currencyRateLayout = new CurrencyRateLayout();
        }
        return currencyRateLayout;
    }
}
