package org.smoofy.ui.upload;

import com.google.common.eventbus.Subscribe;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.smoofy.common.eventbus.TrackerEventBus;
import org.smoofy.common.eventbus.event.AddValueEvent;
import org.smoofy.common.eventbus.event.SuccessfullUpload;
import org.smoofy.model.CurrencyObject;
import org.smoofy.ui.TrackerLayout;

/**
 * Created by Shaman on 4/5/2016.
 */
public class FileUploadLayout extends VerticalLayout {

    private Label label;
    private FileUpload upload;

    public FileUploadLayout(){
        addComponents(getLabel(), getUpload());

        TrackerEventBus.getEventBus().register(this);
    }

    public Label getLabel() {
        if (label == null) {
            label = new Label();
            label.setCaption("Upload .txt file with data");
        }
        return label;
    }

    public FileUpload getUpload() {
        if (upload == null) {
            upload = new FileUpload();
        }
        return upload;
    }

    @Subscribe
    public void successfullUpload(SuccessfullUpload upload){
        UI.getCurrent().setContent(new TrackerLayout());

        for (CurrencyObject object : upload.getResult()) {
            TrackerEventBus.getEventBus().post(new AddValueEvent(object));
        }
    }
}
