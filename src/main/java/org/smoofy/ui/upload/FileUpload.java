package org.smoofy.ui.upload;

import com.vaadin.server.Page;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.MultiFileUpload;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadFinishedHandler;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadStateWindow;
import org.smoofy.common.eventbus.TrackerEventBus;
import org.smoofy.common.eventbus.event.SuccessfullUpload;
import org.smoofy.common.util.CurrencyConverterUtil;
import org.smoofy.model.CurrencyObject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shaman on 4/5/2016.
 */
public class FileUpload extends HorizontalLayout {

    private static final String STYLE_NAME = "upload";

    private MultiFileUpload upload;

    public FileUpload() {
        addComponents(getUpload());
        setPrimaryStyleName(STYLE_NAME);
        upload.setPrimaryStyleName(STYLE_NAME);
    }

    public MultiFileUpload getUpload() {
        if (upload == null) {
            upload = new MultiFileUpload(new UploadFinishedHandler() {
                @Override
                public void handleFile(InputStream inputStream, String s, String s1, long l) {
                    if(inputStream != null){
                        List<CurrencyObject> result = new ArrayList<CurrencyObject>();

                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        String line;
                        CurrencyObject convert;
                        try {
                            while((line = reader.readLine()) != null){
                                convert = CurrencyConverterUtil.convert(line);
                                if(convert != null){
                                    result.add(convert);
                                }
                            }
                            TrackerEventBus.getEventBus().post(new SuccessfullUpload(result));
                        } catch (IOException e) {
                            new Notification("Could not read file<br/>",
                                    e.getMessage(),
                                    Notification.Type.ERROR_MESSAGE)
                                    .show(Page.getCurrent());
                        }
                    }
                }
            }, new UploadStateWindow(), false);
            upload.getSmartUpload().setUploadButtonCaptions("Upload file", "");
        }
        return upload;
    }
}