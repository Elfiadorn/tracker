package org.smoofy.ui.upload;

import com.vaadin.server.Page;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload;

import java.io.*;

/**
 * Created by Shaman on 4/5/2016.
 */
public class FileUploader extends HorizontalLayout {

    private Upload upload;
    private File file;

    public FileUploader() {
        addComponents(getUpload());
    }

    public Upload getUpload() {
        if (upload == null) {
            upload = new Upload("Select TXT file", new Upload.Receiver() {
                @Override
                public OutputStream receiveUpload(String filename, String mimeType) {
                    FileOutputStream fos = null; // Stream to write to
                    try {
                        // Open the file for writing.
                        file = File.createTempFile("temp", ".txt");
                        fos = new FileOutputStream(file);
                    } catch (final java.io.FileNotFoundException e) {
                        new Notification("Could not open file<br/>",
                                e.getMessage(),
                                Notification.Type.ERROR_MESSAGE)
                                .show(Page.getCurrent());
                        return null;
                    } catch (IOException e) {
                        new Notification("Could not create temp file<br/>",
                                e.getMessage(),
                                Notification.Type.ERROR_MESSAGE)
                                .show(Page.getCurrent());
                    }
                    return fos; // Return the output stream to write to
                }
            });
            upload.addSucceededListener(new Upload.SucceededListener() {
                @Override
                public void uploadSucceeded(Upload.SucceededEvent event) {
                    if (file != null) {
                        String content = null;
                        //try to read
                        FileReader reader = null;
                        BufferedReader br = null;
                        try {
                            reader = new FileReader(file);
                            br = new BufferedReader(reader);
                            try {
                                String x;
                                while ((x = br.readLine()) != null) {
                                    // printing out each line in the file
                                    System.out.println(x);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (FileNotFoundException e) {
                            System.out.println(e);
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
        return upload;
    }
}
