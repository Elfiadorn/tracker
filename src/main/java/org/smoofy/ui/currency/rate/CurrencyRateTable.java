package org.smoofy.ui.currency.rate;

import com.google.common.collect.Iterables;
import com.google.common.eventbus.Subscribe;
import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;
import org.smoofy.common.eventbus.TrackerEventBus;
import org.smoofy.common.eventbus.event.CurrencyRateAddEvent;
import org.smoofy.common.filter.CurrencyRateFilter;
import org.smoofy.model.CurrencyRate;
import org.smoofy.model.service.CurrencyService;

/**
 * Created by Shaman on 4/5/2016.
 */
public class CurrencyRateTable extends Grid {

    private static final String STYLE_NAME = "currency-rate-table";

    private BeanItemContainer<CurrencyRate> container;

    public CurrencyRateTable(){
        addStyleName(STYLE_NAME);
        setWidth(100, Unit.PERCENTAGE);

        setContainerDataSource(getContainer());
        setColumnOrder("currency", "comparedCurrency", "rate");

        TrackerEventBus.getEventBus().register(this);
    }

    private BeanItemContainer<CurrencyRate> getContainer() {
        if(container == null){
            container = new BeanItemContainer<CurrencyRate>(CurrencyRate.class, Lists.newArrayList());
        }
        return container;
    }

    @Subscribe
    public void addValue(CurrencyRateAddEvent event){
        CurrencyRate bean = event.getRate();

        CurrencyRate containerBean = findBean(bean);

        if(containerBean == null){
            getContainer().addBean(bean);
        }else{
            BeanItem<CurrencyRate> item = getContainer().getItem(containerBean);
            item.getItemProperty("rate").setValue(bean.getRate());
        }

        CurrencyService.getInstance().insertCurrencyRate(bean);
    }

    private CurrencyRate findBean(CurrencyRate bean){

        Iterable<CurrencyRate> filter = Iterables.filter(getContainer().getItemIds(), new CurrencyRateFilter(bean.getComparedCurrency()));

        //get first object
        CurrencyRate result = null;

        for (CurrencyRate object : filter) {
            if(result == null){
                result = object;
            }else{
                //throw exception
            }
        }

        return result;

    }

}
