package org.smoofy.ui.currency.rate;

import com.google.common.eventbus.Subscribe;
import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import org.smoofy.common.eventbus.TrackerEventBus;
import org.smoofy.common.eventbus.event.AvailableCurrenciesChangeEvent;
import org.smoofy.common.eventbus.event.CurrencyRateAddEvent;
import org.smoofy.model.CurrencyRate;
import org.smoofy.model.service.CurrencyService;

import java.util.Set;

/**
 * Created by Shaman on 4/5/2016.
 */
public class CurrencyRateLayout extends VerticalLayout {

    private CurrencyRateTable rateTable;
    private TextField usdTextField;
    private ComboBox currency;
    private TextField rate;
    private NativeButton add;

    public CurrencyRateLayout(){
        addComponents(getRateTable(), getAddRow());

        TrackerEventBus.getEventBus().register(this);
    }

    public HorizontalLayout getAddRow(){
        HorizontalLayout layout = new HorizontalLayout();
        layout.addComponents(getUsdTextField(), getCurrency(), getRate(), getAdd());
        return layout;
    }

    public CurrencyRateTable getRateTable() {
        if (rateTable == null) {
            rateTable = new CurrencyRateTable();
        }
        return rateTable;
    }

    public TextField getUsdTextField() {
        if (usdTextField == null) {
            usdTextField = new TextField();
            usdTextField.setValue("USD");
            usdTextField.setEnabled(false);
        }
        return usdTextField;
    }

    public ComboBox getCurrency() {
        if (currency == null) {
            currency = new ComboBox();
            currency.addValidator(new Validator() {
                @Override
                public void validate(Object value) throws InvalidValueException {
                    String val = (String) value;
                    if(val == null || "".equals(val)){
                        throw new InvalidValueException("Currency need to be selected");
                    }
                }
            });
            currency.setValidationVisible(false);
            currency.setNullSelectionAllowed(false);

            CurrencyService service = CurrencyService.getInstance();
            Set<String> availableCurrencies = service.getAvailableCurrencies();

            for (String cur : availableCurrencies) {
                currency.addItem(cur);
            }

        }
        return currency;
    }

    public TextField getRate() {
        if (rate == null) {
            rate = new TextField();
            rate.setValue("0");
            rate.addValidator(new Validator() {
                @Override
                public void validate(Object value) throws InvalidValueException {
                    String val = (String) value;
                    try {
                        Float.parseFloat(val);
                    }catch (NumberFormatException nfe){
                        throw new InvalidValueException("Wrong rate format. Please use numbers");
                    }
                }
            });
        }
        return rate;
    }

    public NativeButton getAdd() {
        if (add == null) {
            add = new NativeButton("ADD");
            add.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    try {
                        //validate widgets
                        currency.validate();
                        rate.validate();

                        //create object
                        CurrencyRate rateObject = new CurrencyRate();
                        rateObject.setComparedCurrency((String) currency.getValue());
                        rateObject.setRate(Float.parseFloat(rate.getValue()));

                        //fire through event bus
                        TrackerEventBus.getEventBus().post(new CurrencyRateAddEvent(rateObject));
                    }catch (Validator.InvalidValueException ive){
                        Notification.show(ive.getLocalizedMessage(), Notification.Type.WARNING_MESSAGE);
                    }
                }
            });
        }
        return add;
    }

    @Subscribe
    public void currenciesDataSourceChangeEvent(AvailableCurrenciesChangeEvent event){
        CurrencyService service = CurrencyService.getInstance();
        Set<String> availableCurrencies = service.getAvailableCurrencies();

        for (String cur : availableCurrencies) {
            currency.addItem(cur);
        }
    }
}
