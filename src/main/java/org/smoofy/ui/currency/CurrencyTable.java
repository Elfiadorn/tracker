package org.smoofy.ui.currency;

import com.google.common.collect.Iterables;
import com.google.common.eventbus.Subscribe;
import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;
import org.smoofy.common.eventbus.TrackerEventBus;
import org.smoofy.common.eventbus.event.AddValueEvent;
import org.smoofy.common.eventbus.event.CurrencyRateAddEvent;
import org.smoofy.common.filter.CurrencyFilter;
import org.smoofy.common.util.CurrencyConverterUtil;
import org.smoofy.model.CurrencyObject;
import org.smoofy.model.CurrencyRate;
import org.smoofy.model.CurrencyTableObject;
import org.smoofy.model.service.CurrencyService;

/**
 * Created by Shaman on 4/4/2016.
 */
public class CurrencyTable extends Grid {

    private static final String STYLE_NAME = "currency-table";

    private BeanItemContainer<CurrencyTableObject> container;

    public CurrencyTable(){
        addStyleName(STYLE_NAME);
        setWidth(100, Unit.PERCENTAGE);

        setContainerDataSource(getContainer());
        setColumnOrder("currency", "currentValue", "inDepositValue", "convertedCurrency");

        getColumn("convertedCurrency").setHeaderCaption("Converted Currency (USD)");

        TrackerEventBus.getEventBus().register(this);
    }

    private BeanItemContainer<CurrencyTableObject> getContainer() {
        if(container == null){
            container = new BeanItemContainer<CurrencyTableObject>(CurrencyTableObject.class, Lists.newArrayList());
        }
        return container;
    }

    @Subscribe
    public void addValue(AddValueEvent event){
        CurrencyObject object = event.getCurrencyObject();

        CurrencyTableObject bean = CurrencyConverterUtil.convert(object);
        CurrencyTableObject containerBean = findBean(bean);

        if(containerBean == null){
            getContainer().addBean(bean);
        }else{
            BeanItem<CurrencyTableObject> item = getContainer().getItem(containerBean);
            item.getItemProperty("inDepositValue").setValue(containerBean.getInDepositValue() + object.getValue());
        }
    }

    @Subscribe
    public void currencyRateAddEvent(CurrencyRateAddEvent event){
        CurrencyRate rate = event.getRate();

        CurrencyTableObject bean = new CurrencyTableObject();
        bean.setCurrency(rate.getComparedCurrency());

        bean = findBean(bean);

        if(bean != null){
            BeanItem<CurrencyTableObject> item = getContainer().getItem(bean);
            item.getItemProperty("convertedCurrency").setValue(bean.getCurrentValue() / rate.getRate());
        }
    }

    private CurrencyTableObject findBean(CurrencyTableObject bean){
        Iterable<CurrencyTableObject> filter = Iterables.filter(getContainer().getItemIds(), new CurrencyFilter(bean.getCurrency()));

        //get first object
        CurrencyTableObject result = null;

        for (CurrencyTableObject object : filter) {
            if(result == null){
                result = object;
            }else{
                //throw exception
            }
        }

        return result;
    }

    public void refresh() {
        for (CurrencyTableObject item : getContainer().getItemIds()) {
            BeanItem<CurrencyTableObject> beanItem = getContainer().getItem(item);
            beanItem.getItemProperty("currentValue").setValue(item.getCurrentValue() + item.getInDepositValue());
            beanItem.getItemProperty("inDepositValue").setValue(0);

            try {
                if (!"USD".equals(item.getCurrency().toUpperCase())) {
                    Float rate = CurrencyService.getInstance().findRate(item.getCurrency());
                    if(rate != null){
                        beanItem.getItemProperty("convertedCurrency").setValue(item.getCurrentValue() / rate);
                        return;
                    }
                }

                beanItem.getItemProperty("convertedCurrency").setValue(0f);
            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }
}
