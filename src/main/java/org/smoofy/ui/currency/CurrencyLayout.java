package org.smoofy.ui.currency;

import com.vaadin.ui.VerticalLayout;
import org.smoofy.ui.upload.FileUploadLayout;

/**
 * Created by Shaman on 4/5/2016.
 */
public class CurrencyLayout extends VerticalLayout {

    private CurrencyTable table;
    private FileUploadLayout upload;

    public CurrencyLayout(){
        addComponents(getTable(), getUpload());

        setExpandRatio(getUpload(), 0.2f);

        setSpacing(true);

        setSizeFull();
    }

    public CurrencyTable getTable() {
        if (table == null) {
            table = new CurrencyTable();
        }
        return table;
    }

    public FileUploadLayout getUpload() {
        if (upload == null) {
            upload = new FileUploadLayout();
        }
        return upload;
    }
}
