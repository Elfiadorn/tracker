package org.smoofy.ui.console;

import com.vaadin.event.FieldEvents;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.ui.*;
import org.smoofy.common.eventbus.TrackerEventBus;
import org.smoofy.common.eventbus.event.AddValueEvent;
import org.smoofy.common.util.CurrencyConverterUtil;
import org.smoofy.model.CurrencyObject;

/**
 * Created by Shaman on 4/4/2016.
 */
public class Console extends VerticalLayout {

    private static final String ERROR_MESSAGE = "Wrong input. Please use format CURRENCY AMOUNT {for example USD 1000}";
    private static final String STYLE_NAME = "console";

    private TextField textField;
    private TextArea textArea;
    private Button commit;

    public Console() {
        addComponents(getTextField(), getTextArea());

        getTextField().addStyleName(STYLE_NAME);
        getTextArea().addStyleName(STYLE_NAME);

        handleEnter();
    }

    public void handleEnter() {
        ShortcutListener enterShortCut = new ShortcutListener("EnterShortcut", ShortcutAction.KeyCode.ENTER, null) {
            @Override
            public void handleAction(Object sender, Object target) {
                String value = getTextField().getValue();

                CurrencyObject convert = CurrencyConverterUtil.convert(value);
                if(convert != null) {

                    //clear text field
                    getTextField().clear();

                    //add new value to text area
                    updateTextArea(value);

                    fireAddValueEvent(convert);
                }else{
                    Notification.show(ERROR_MESSAGE, Notification.Type.ERROR_MESSAGE);
                }
            }
        };

        handleShortcut(enterShortCut);
    }

    private void updateTextArea(String value) {
        String text = getTextArea().getValue();

        if(!text.isEmpty()) {
            text += "\n" + value;
        }else{
            text += value;
        }

        getTextArea().setValue(text);
    }

    private void fireAddValueEvent(CurrencyObject object) {
        TrackerEventBus.getEventBus().post(new AddValueEvent(object));
    }

    /**
     * Lets the ShortcutListener handle the action while the TextField has focus.
     */
    public void handleShortcut(ShortcutListener shortcutListener) {
        getTextField().addFocusListener(new FieldEvents.FocusListener() {
            @Override
            public void focus(FieldEvents.FocusEvent event) {
                getTextField().addShortcutListener(shortcutListener);
            }
        });

        getTextField().addBlurListener(new FieldEvents.BlurListener() {
            @Override
            public void blur(FieldEvents.BlurEvent event) {
                getTextField().removeShortcutListener(shortcutListener);
            }
        });

        getTextArea().addFocusListener(new FieldEvents.FocusListener() {
            @Override
            public void focus(FieldEvents.FocusEvent event) {
                getTextField().focus();
            }
        });
    }

    public TextField getTextField() {
        if (textField == null) {
            textField = new TextField();
            textField.setSizeFull();
        }
        return textField;
    }

    public TextArea getTextArea() {
        if(textArea == null){
            textArea = new TextArea();
            textArea.setSizeFull();
        }
        return textArea;
    }
}
